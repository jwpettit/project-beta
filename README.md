# AutoBahn

Team:

* Person 1 - Zoybh Sales
* Person 2 - Jeff Services

This project is a fully functioning car dealership/shop webapp. There are 3 services we cover which include Sales, Inventory, and Service.

**Features**

- Create & List Manufacturers
- Create & List Vehicle Models
- Create & List Automobiles in the Inventory
- Create Technicians
- Create, List, Delete, and mark as complete Appointments
- The appointments list page will will filter out any already completed appointments and also show if the car had been sold by us as indicated with the VIP section.
- Search for past Services by VIN
- Create sales person
- Create potential customers
- Create a sales record and list all sales
- Select a sales person from a drop down list and view detail of their sales

**Diagram:**

<a href="https://imgur.com/XZGBGq4"><img src="https://i.imgur.com/XZGBGq4.png" title="source: imgur.com" /></a>

**Set up:**

**Clone the Repo:**

- Open terminal
- Make a directory: mkdir “Name of your choice”
- Clone the repo below: git clone “repo link”

[](https://gitlab.com/jwpettit/project-beta.git)

**Install and run docker:**

[Docker: Accelerated, Containerized Application Development](https://www.docker.com/)

**Within vs code run the following commands:**

1. docker volume create beta-data
2. docker-compose build
3. docker-compose up

Wait for the docker to build containers and images. This could take up to 5 mins or more.

Once docker is fully up and running:

<a href="https://imgur.com/Mqn5qUX"><img src="https://i.imgur.com/Mqn5qUX.png" title="source: imgur.com" /></a>

It should look like this.

**You can access the Webapp at :** localhost:3000

**The webapp is broken into 3 sections:**

**Service:**

**Service API Overview**

The service API tracks service appointments for cars and owners

**Technician Tracking:** [http://localhost:3000/technicians/new/](http://localhost:3000/technicians/new/)

A form allows a user to enter a technician’s identification details like name and employee number. This form is accessible through the nav bar.

**Appointment Tracking:** [http://localhost:3000/serviceappointments/](http://localhost:3000/serviceappointments/) , [http://localhost:3000/serviceappointments/](http://localhost:3000/serviceappointments/)

A form allows a service advisor to enter the details of a vehicle and it’s customer, such as: VIN, customer name, date/time of appointment, technician assigned, and reason for visit. This form is accessible through the nav bar.

The service section of the app shows a list of appointments, including all of the details related to the appointment. The list also includes the option to cancel an appointment or finish it (need to look up some details on how to handle this).

**Service History:** [http://localhost:3000/servicehistory/](http://localhost:3000/servicehistory/)

Within the service app is also the ability to track a car’s service history by VIN number. The list view for this functionality is identical to the appointment tracking view, but shows appointments for a specific car both, both past and present.

**Sales:**

The Sales Api allows you to track customers, sales made, as well as adding and search old and new sales

**Sales API Overview**

**New Customer:** [http://localhost:3000/customers/new/](http://localhost:3000/customers/new/)

Ability to add a new customer to the system

**New Sales Representative:** [http://localhost:3000/sales-persons/new/](http://localhost:3000/sales-persons/new/)

Add new employees to the system

**New Sales:** [http://localhost:3000/sales-records/new/](http://localhost:3000/sales-records/new/)

Keep track of new sales being made.

**All Sales:** [http://localhost:3000/sales-records/](http://localhost:3000/sales-records/)

Keep track of all the sales done.

**Sales by Employee:** [http://localhost:3000/sales-records/filter/](http://localhost:3000/sales-records/filter/)

Keep track of sales done by each employee.

**Inventory:**

**Inventory API Overview:**

The Inverntory API allows you to add new vehicles, view all available vehicles, add and view manufacturers, as well as add and view models.

**New Manufaturer:** [http://localhost:3000/manufacturers/new/](http://localhost:3000/manufacturers/new/)

Add a new car brand.

**All Manufacturer:** [http://localhost:3000/manufacturers/](http://localhost:3000/manufacturers/)

View all car brands in the database.

**New Vehicle Model:** [http://localhost:3000/models/new/](http://localhost:3000/models/new/)

Add a new vehicle model

**All Models:** [http://localhost:3000/models/](http://localhost:3000/models/)

View all vehicle models

**New Automobile:** [http://localhost:3000/vehicles/new/](http://localhost:3000/vehicles/new/)

Add a new car that comes in.

**All Automobiles:** [http://localhost:3000/vehicles/](http://localhost:3000/vehicles/)

View all the Cars available in the dealership.
