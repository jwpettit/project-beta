import React from 'react'
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import { NavLink } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner';

export default class VehicleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: '',
            year: '',
            vin: '',
            hasEntered: false,
            error: false,
        };

        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleVINChange = this.handleVINChange.bind(this);
        this.handleYearChange = this.handleYearChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleYearChange(event) {
        const value = event.target.value;
        this.setState({ year: value })
    }

    handleVINChange(event) {
        const value = event.target.value;
        this.setState({ vin: value })
    }

    handleModelChange(event) {
        const value = event.target.value;
        this.setState({ model_id: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.models;
        delete data.hasEntered;
        delete data.error;

        const createAutomobile = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const cleared = {
            color: '',
            year: '',
            vin: '',
            model_id: '',
            hasEntered: true,
            error: false,
        }

        const response = await fetch(createAutomobile, fetchConfig);
        if (response.ok) {
            this.setState(cleared);
            let newVehicle = await response.json();
            this.props.addVehicle(newVehicle);
        } else {
            this.setState({
                hasEntered: false,
                error: true,
            })
        }
    }

    render() {
        let spinnerClasses = '';
        let promptClasses = false;
        let dropdownClasses = 'form-select d-none';
        if (this.props.models?.length > 0) {
            spinnerClasses = 'd-none';
            promptClasses = false;
            dropdownClasses = 'form-select';
        } else {
            dropdownClasses = 'form-select d-none';
            spinnerClasses = 'd-none';
            promptClasses = true;
        }

        let successMessageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            successMessageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
            setTimeout(() => {
                successMessageClasses = 'alert alert-success d-none mb-0';
                formClasses = '';
                this.setState({ hasEntered: false })
            }, 2000);
        }

        let errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
        if (this.state.error) {
            errorMessageClasses = 'alert alert-danger mb-0 mt-3';
            setTimeout(() => {
                errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
                this.setState({ error: false })
            }, 5000);
        }

        return (
            <>
                <div className='my-5 container'>
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className='card-body'>
                                    <h1 className="card-title">Add a New Inventory Vehicle</h1>
                                    <p className="mb-3">
                                        Please add a new inventory vehicle (note: VIN no. must be unique)
                                    </p>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-automobile-form">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                                <label htmlFor="color">Color</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleYearChange} value={this.state.year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                                                <label htmlFor="year">Year</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleVINChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                                <label htmlFor="vin">VIN No. (17 Characters)</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <Spinner animation="border" className={spinnerClasses} />
                                        </div>
                                        <div>
                                            <Alert show={promptClasses} variant="warning">
                                                <Alert.Heading>Uh oh, we have no vehicle models!</Alert.Heading>
                                                <p>
                                                    Please add a vehicle model to the database in order to proceed with adding an inventory vehicle.
                                                </p>
                                                <hr />
                                                <div className="d-flex justify-content-end">
                                                    <Button as={NavLink} to="/models/new/" variant="outline-warning">
                                                        Add a Model
                                                    </Button>
                                                </div>
                                            </Alert>
                                        </div>
                                        <div className="col">
                                            <div className="mb-3">
                                                <select onChange={this.handleModelChange} placeholder="Model" required name="model_id" id="model_id" className={dropdownClasses}>
                                                    <option value="">Choose a Model</option>
                                                    {this.props.models?.map(auto => {
                                                        return (
                                                            <option key={auto.id} value={auto.id}>
                                                                {auto.manufacturer.name} - {auto.name}
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                            </div>
                                        </div>
                                        <button className="btn btn-outline-dark">Add Inventory Vehicle</button>
                                    </form>
                                    <div className={successMessageClasses} id="success-message">
                                        Inventory vehicle has been added!
                                    </div>
                                    <div className={errorMessageClasses} id="error-message">
                                        Entered data not valid!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
