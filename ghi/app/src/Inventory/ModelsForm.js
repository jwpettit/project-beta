import React from 'react'
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import { NavLink } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner';

export default class VehicleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            picture_url: '',
            hasEntered: false,
            error: false,
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.manufacturers;
        delete data.hasEntered;
        delete data.error;

        const createVehicleModel = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const cleared = {
            name: "",
            picture_url: "",
            manufacturer_id: "",
            hasEntered: true,
            error: false,
        }
        const response = await fetch(createVehicleModel, fetchConfig);
        if (response.ok) {
            this.setState(cleared);
            let newModel = await response.json();
            this.props.addModel(newModel);
        } else {
            this.setState({
                hasEntered: false,
                error: true,
            })
        }
    }


    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }
    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer_id: value });
    }


    render() {
        let spinnerClasses = '';
        let promptClasses = false;
        let dropdownClasses = 'form-select d-none';
        if (this.props.manufacturers?.length > 0) {
            spinnerClasses = 'd-none';
            promptClasses = false;
            dropdownClasses = 'form-select';
        } else {
            dropdownClasses = 'form-select d-none';
            spinnerClasses = 'd-none';
            promptClasses = true;
        }

        let successMessageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasEntered) {
            successMessageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
            setTimeout(() => {
                successMessageClasses = 'alert alert-success d-none mb-0';
                formClasses = '';
                this.setState({ hasEntered: false })
            }, 2000);
        }

        let errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
        if (this.state.error) {
            errorMessageClasses = 'alert alert-danger mb-0 mt-3';
            setTimeout(() => {
                errorMessageClasses = 'alert alert-danger d-none mb-0 mt-3';
                this.setState({ error: false })
            }, 5000);
        }

        return (
            <>
                <div className='my-5 container'>
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className='card-body'>
                                    <h1 className="card-title">Add a New Model</h1>
                                    <p className="mb-3">
                                        Please add a new vehicle model
                                    </p>
                                    <form className={formClasses} onSubmit={this.handleSubmit} id="create-vehicleModel-form">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                                <label htmlFor="name">Name</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handlePictureUrlChange} value={this.state.picture_url} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                                                <label htmlFor="picture_url">Picture URL (max 200 characters)</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <Spinner animation="border" className={spinnerClasses} />
                                        </div>
                                        <div>
                                            <Alert show={promptClasses} variant="warning">
                                                <Alert.Heading>Uh oh, there are no manufacturers in the system!</Alert.Heading>
                                                <p>
                                                    Please add a vehicle manufacturer to the database in order to proceed with adding a vehicle model.
                                                </p>
                                                <hr />
                                                <div className="d-flex justify-content-end">
                                                    <Button as={NavLink} to="/manufacturers/new/" variant="outline-warning">
                                                        Add a Manufacturer
                                                    </Button>
                                                </div>
                                            </Alert>
                                        </div>
                                        <div className="col">
                                            <div className="mb-3">
                                                <select onChange={this.handleManufacturerChange} placeholder="Choose a Manufacturer" required name="manufacturer_id" id="manufacturer_id" className={dropdownClasses}>
                                                    <option value="">Choose a Manufacturer</option>
                                                    {this.props.manufacturers?.map(manufacturer => {
                                                        return (
                                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                                {manufacturer.name}
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                            </div>
                                        </div>
                                        <button className="btn btn-outline-dark">Add Model</button>
                                    </form>
                                    <div className={successMessageClasses} id="success-message">
                                        Vehicle model has been added!
                                    </div>
                                    <div className={errorMessageClasses} id="error-message">
                                        Entered data not valid!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
