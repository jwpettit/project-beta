import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/esm/Button';

export default function CustomerList({ customers, deleteCustomer }) {
    return (
        <>
            <h1>All Customers</h1>
            <Table striped hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone No.</th>
                        <th align='right' width="100px"></th>
                    </tr>
                </thead>
                <tbody>
                    {customers && customers.map(customer => {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone_number}</td>
                                <td>
                                    <Button variant="danger" onClick={() => deleteCustomer(`${customer.id}`)}>Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    )
}
